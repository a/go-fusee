# go-fusee

Re-implementation of the Fusee Gelee exploit in Go, aimed to be lightweight and cross-platform.

Currently it only works properly on macOS (which was the primary goal of this project as a compiled, lightweight does not exist for MacOS, while Linux has fusee-nano and Windows has tegrarcmsmash), but I'm working on bringing support for Linux too. If you're looking for something that works on linux *now*, then check out [fusee-nano](https://github.com/DavidBuchanan314/fusee-nano), it's *amazing*.

### Setting up

Simply run `go build`, and a binary with the name `go-fusee` should appear.

### Usage

The argument format is `go-fusee [payload path]`.

On macOS: There's nothing to keep in mind. Just run the binary with `./go-fusee [payload path]`.

On Linux: You might need to set up udev rules or run with sudo. I'll document these further when I push linux support.

### Acknowledgements

- fusee-nano, as I based some of the code on it and as it helped me cut down on unnecessary code
- fusee-launcher, as I based a bit of the code on it

