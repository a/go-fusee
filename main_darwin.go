// go-fusee
// Copyright (C) 2019 Arda "Ave" Ozkal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
    "fmt"
    "github.com/google/gousb"
)

func doMemcpy(dev *gousb.Device, length int) {
    var data = make([]byte, length)
    fmt.Println("Smashing stack...")
    out, err := dev.Control(standardRequestDeviceToHostToEndpoint, getStatus, 0, 0, data)
    if err != nil {
        if out == -7 {
            fmt.Println("Stack smashing successful, payload launched. Have fun!")
        } else {
            fmt.Println("Stack smashing failed: ", err)
        }
    }
}